﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCAutentimisega.Models;

namespace MVCAutentimisega.Controllers
{
    public class FilesController : Controller
    {
        private MartinEntities db = new MartinEntities();

        public ActionResult Get(int? id)
        {
            if (! id.HasValue) return HttpNotFound();
            File file = db.Files.Find(id.Value);
            if (file == null) return HttpNotFound();
            return File(file.Content, file.ContentType);
        }
    }
}
