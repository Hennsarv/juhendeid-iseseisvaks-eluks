﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCAutentimisega.Models;

namespace MVCAutentimisega.Models
{
    partial class Person
    {
        public static Dictionary<int, string> States = new Dictionary<int, string>
        {
            {0, "ootel" },
            {1, "aktiivne" },
            {2, "puhkusel" },
            {3, "pensionil" },
            {4, "lahkunud" },
        };
        public static Dictionary<int, string> StatesEnglish = new Dictionary<int, string>
        {
            {0, "waiting" },
            {1, "active" },
            {2, "resting" },
            {3, "retired" },
            {4, "left" },
        };
        public static Dictionary<int, string> StatesFinnish = new Dictionary<int, string>
        {
            {0, "odotamassa" },
            {1, "toiminnassa" },
            {2, "lomalla" },
            {3, "eläkkeellä" },
            {4, "positunut" },
        };


        public string State =>
            //ConfigurationManager.AppSettings["Language"] == "English" ? StatesEnglish[this.PersonState] :
            //ConfigurationManager.AppSettings["Language"] == "Finnish" ? StatesFinnish[this.PersonState] :
            
            States.ContainsKey(PersonState) ? States[this.PersonState] : "segane olek"
            ;



        static MartinEntities db = new MartinEntities();

        public string FullName => FirstName + " " + LastName;

        public static Person GetByEmail(string email)
            => db.People.Where(x => x.Email == email).SingleOrDefault();

        public bool IsInRole(string rolename)
            => this.UserInRoles
                .Select(x => x.Role.RoleName.ToLower()).ToList()
                .Intersect(rolename.Split(',').Select(y => y.Trim().ToLower()))
                .Count() > 0;

    }
}

namespace MVCAutentimisega.Controllers
{

    public class PeopleController : Controller
    {
        private MartinEntities db = new MartinEntities();
        

        // GET: People
        public ActionResult Index()
        {
            return View(db.People.ToList());
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Create()
        {
            ViewBag.PersonState = new SelectList(Person.States, "Key", "Value");
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName");
            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PersonID,FirstName,LastName,StartDate,PersonState,DepartmentID,Picture,Email")] Person person)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.People.Add(person);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    ViewBag.ErrorMessage = "sellise isikukoodi või emailiga vist on juba keegi";
                    
                }
            }
            ViewBag.PersonState = new SelectList(Person.States, "Key", "Value", person.PersonState);
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName", person.Department);
            return View(person);
        }

        // POST: People/AddPicture/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPicture([Bind(Include = "Id")]Person person0, HttpPostedFile pilt)
        {
            int id = person0.Id;
            Person person = db.People.Find(id);
            if (person == null) return RedirectToAction("Index");
            if (pilt == null || pilt.ContentLength == 0) return RedirectToAction("Details", new { id = id });
            File newFile = new File();
            using (System.IO.BinaryReader br = new System.IO.BinaryReader(pilt.InputStream))
            {
                byte[] buff = br.ReadBytes(pilt.ContentLength);
                newFile.Content = buff;
            }
            db.SaveChanges();
            person.Picture = newFile.Id;
            db.SaveChanges();


            return RedirectToAction("Details", new { id = id });
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            ViewBag.PersonState = new SelectList(Person.States, "Key", "Value", person.PersonState);

            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName", person.Department);

            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PersonID,FirstName,LastName,StartDate,PersonState,DepartmentID,Email")] Person person, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.Entry(person).Property(x => x.Picture).IsModified = false;
                db.SaveChanges();

                if (file != null && file.ContentLength > 0)
                {
                    using (System.IO.BinaryReader br = new System.IO.BinaryReader(file.InputStream))
                    {
                        // ma lihtsuse mõttes kustutan vana pildi baasist
                        File v = db.Files.Find(person.Picture ?? 0);
                        if (v != null)
                        {
                            db.Files.Remove(v);
                            db.SaveChanges();
                        }
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        File f = new File { Content = buff, ContentType = file.ContentType, Filename = file.FileName.Split('\\').Last().Split('/').Last() };
                        db.Files.Add(f);
                        db.SaveChanges();
                        person.Picture = f.Id;
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            ViewBag.PersonState = new SelectList(Person.States, "Key", "Value", person.PersonState);
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName", person.Department);
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
