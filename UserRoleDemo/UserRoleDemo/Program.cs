﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserRoleDemo
{
    static class Program
    {

        static string SplitAndJoin(this string tekst, char eraldaja, Func<string,string> f)
        => string.Join(eraldaja.ToString(),tekst.Split(eraldaja).Select(f));

        static string MultiToProper(this string nimi) // ilus
            => nimi == "" ? "" :
                nimi.SplitAndJoin(' ',
                    x => x.SplitAndJoin('-', 
                        y => y.Substring(0, 1).ToUpper() + y.Substring(1).ToLower()));

        static string MultiToProperKole(this string nimi)
            => nimi == "" ? "" :
            string.Join(" ",
                    nimi
                        .Split(' ')
                        .Select(x => string.Join("-", 
                                x.Split('-')
                                .Select(y => y.Substring(0,1).ToUpper()+y.Substring(1).ToLower())))
                );

        static void Main(string[] args)
        {

            Console.WriteLine("MultiToProperDemo");
            string[] nimed =
            {
                "imelikNIMI",
                "imelik nIMI-sideKriips",
                "ImeLIK-nimi SIDE-kriips"
            };
            Console.WriteLine();

            nimed.Select(x => x.MultiToProper()).ToList().ForEach(x => Console.WriteLine(x));


            tydrukudEntities db = new tydrukudEntities();
            db.Database.Log = Console.WriteLine;

            db.People.Include("UserInRoles")
                .AsEnumerable()
                .Select(x => new {
                    x.FirstName,
                    x.LastName,
                    MainRole = x.UserInRoles.FirstOrDefault()?.Role.RoleName })
                .ToList()
                .ForEach(x => Console.WriteLine(x));
        }
    }
}
