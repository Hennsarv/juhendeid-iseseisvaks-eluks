﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelWork
{
    class Program
    {
        static Random r = new Random();

        static async Task Prindi(int mitu, object mida)
        {
            Console.WriteLine($"alustan {mida}");
            await Task.Delay(1);
            for (int i = 0; i < mitu; i++)
            {
                Console.WriteLine($"prindin {mida} {i+1}. korda {DateTime.Now.ToLongTimeString()}");
                System.Threading.Thread.Sleep(r.Next(0, 2000));
            }
            Console.WriteLine($"lõpetan {mida}");

        }

        static void Main() => AsyncMain();

        static async void AsyncMain()
        {
            var arvud = Enumerable.Range(0, 100);

            arvud.ToList()
                .AsParallel()
                .ForAll(x => Prindi(1, x));

            Console.ReadKey();

        }
    }
}
