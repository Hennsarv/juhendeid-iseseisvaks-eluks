﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnDynamic
{

    class Bag : DynamicObject
    {
        IDictionary<string, object> Prop = new Dictionary<string, dynamic>();

       

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = Prop.Keys.Contains(binder.Name) ?
                    Prop[binder.Name] : null;
            return true;
        }
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            Prop[binder.Name] = value;
            return true;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            dynamic d = 7;
            d++;
            Console.WriteLine(d.GetType().Name);
            d = "Henn";

            dynamic seljakott = new Bag();

            seljakott.kaal = 77;
            seljakott.kaal += 10;
            seljakott.tootja = "Lancoste";

            Console.WriteLine($"Selle seljakoti on tootnud {seljakott.tootja} ja ta kaalub {seljakott.kaal}");

        }
    }
}
