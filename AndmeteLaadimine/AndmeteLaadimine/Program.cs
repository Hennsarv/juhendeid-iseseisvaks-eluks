﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AndmeteLaadimine
{
    class Program
    {
        static void Main(string[] args)
        {
            tydrukudEntities db = new tydrukudEntities();
            string filename = @"c:\henn\osakonnad.csv";
            string[] osakonnadtxt = File.ReadAllLines(filename);
            foreach(var x in osakonnadtxt
                .Skip(1)
                .Select( x => x.Split(';'))
                .Select( x => new {Nimi = x[0], kirjeldus = x[1]})
                ) //Console.WriteLine(x);
            {
                db.Departments.Add(new Department { DepartmentName = x.Nimi });
                db.SaveChanges();
            }
        }
    }
}
