﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DatePickerDemo3.Controllers
{
    public class HomeController : MyController
    {


        public ActionResult Index()
        {


            ViewBag.AppStarted = AppBag.Started;
            ViewBag.SessStarted = SessBag.Started;
            ViewBag.AppRequestNo = AppBag.Request++;
            ViewBag.SessRequestNo = SessBag.Request++;
            ViewBag.SessNr = SessBag.Nr;

            ViewBag.Name = ParamsBag.Name;

            #region cookie
            HttpCookie h;
            h = Request.Cookies["test"] ?? new HttpCookie("test");
            ViewBag.PreviousName = h?.Values["name"] ?? "puudub";
            
            if (ParamsBag.Name != null)
            {
                h.Values["name"] = ParamsBag.Name;
                h.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(h);
            }
            #endregion

            return View();
        }

        [OutputCache(Duration = 60, VaryByParam = "who")]
        public ActionResult About(string who = "no one")
        {
            ViewBag.Message = "Your application description page.";
            ViewBag.Time = DateTime.Now.ToLongTimeString();
            ViewBag.Who = who;


            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            ViewBag.Nimekiri = Nimekiri;

            return View();
        }
    }
}