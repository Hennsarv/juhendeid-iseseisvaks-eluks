﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;       // selle rea palus readme lisada, kui puudub
using System.Web.Http;          // selle rea palus readme lisada, kui puudub


namespace DatePickerDemo3
{
    public class MvcApplication : System.Web.HttpApplication
    {
        int sessionNr = 0;
        protected void Application_Start()  // meil oli Application_Start olemas
        {
            GlobalConfiguration.Configure(WebApiConfig.Register); // selle rea palus readme lisada

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            GlobalConfiguration.Configuration.Formatters
                .JsonFormatter
                .SerializerSettings
                .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            //teine eemaldab standardse XML-formatteri
            GlobalConfiguration.Configuration.Formatters
                .Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);




            Application["Started"] = DateTime.Now.ToLongTimeString();
            Application["Request"] = 0;
        }

        protected void Session_Start()
        {
            Session["Started"] = DateTime.Now.ToLongTimeString();
            Session["Nr"] = ++sessionNr;
            Session["Request"] = 0;
        }
    }
}
