﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Caching;
using DatePickerDemo3.Models;

namespace DatePickerDemo3
{

    public class MyController : Controller
    {
        static int mitmes = 0;
        public dynamic TempBag => new UniversalBag(this.TempData);

        public dynamic AppBag => new UniversalBag(System.Web.HttpContext.Current.Application);
        public dynamic SessBag => new UniversalBag(System.Web.HttpContext.Current.Session);
        public dynamic ParamsBag => new UniversalBag(Request.Params);

        Cache cache = System.Web.HttpContext.Current.Cache;
        dynamic CacheBag => new UniversalBag(cache);

        public List<string> Nimekiri
        {
            get
            {
                
                List<string> nimekiri = CacheBag.Nimekiri as List<string>;
                if (nimekiri == null)
                {
                    nimekiri = new List<string>
                {
                    "Henn",
                    "Ants",
                    "Peeter",
                    $"nimi {++mitmes}"
                };
                    //cache["nimekiri"] = nimekiri;
                    TimeSpan ts = new TimeSpan(0, 0, 60);
                    cache.Add("Nimekiri", value: nimekiri, dependencies: null, slidingExpiration: TimeSpan.Zero, priority: CacheItemPriority.Default, absoluteExpiration: DateTime.Now.AddMinutes(1), onRemoveCallback: null);
                }
                return nimekiri;
            }
        }

        public List<Employee> Employees {
            get {
                List<Employee> employees = CacheBag.Employees as List<Employee>;
                if (employees == null)
                {
                    using (NorthwindEntities db = new NorthwindEntities())
                    {
                        employees = db.Employees.ToList();
                        CacheBag.Employees = employees;
                    }
                }
                return employees;
            } }

    }

    
 

    public class UniversalBag : DynamicObject
    {
        dynamic Prop;

        public UniversalBag(object prop)
        {
            Prop = prop;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            try
            {
                result = Prop[binder.Name];
            }
            catch { result = null; }
            return true;
        }
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            Prop[binder.Name] = value;
            return true;
        }
    }


}